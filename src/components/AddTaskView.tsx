import * as React from 'react';

export default class AddTaskView extends React.Component<any,any> {
    constructor(props) {
        super(props)
        this.watchForSubmit = this.watchForSubmit.bind(this)
    }

    watchForSubmit(e) {
        if (e.key === 'Enter' && e.target.value.trim().length) {
            this.props.addTask(e.target.value)
            e.target.value = ''
        }
    }

    render() {
        return <input className="new-todo" placeholder="What needs to be done?" autoFocus={true}
                      onKeyUp={this.watchForSubmit}/>;
    }
}