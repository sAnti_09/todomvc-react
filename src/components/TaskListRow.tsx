import * as React from 'react';
import {Task} from "../models/todo";
import ReactElement = React.ReactElement;

export default class TaskListRow extends React.Component<any,any> {
    constructor(props) {
        super(props)
        this.toggleDone = this.toggleDone.bind(this)
        this.remove = this.remove.bind(this)
        this.edit = this.edit.bind(this)
        this.finishEdit = this.finishEdit.bind(this)
    }

    toggleDone() {
        this.props.toggleDone(this.props.task.id)
    }

    remove() {
        this.props.remove(this.props.task.id)
    }

    edit() {
        this.props.edit(this.props.task.id)
    }

    finishEdit(e) {
        if (e.key === 'Enter') {
            let task: Task = this.props.task
            task.description = e.target.value
            this.props.finishEdit(task)
        } else if (e.keyCode == 27)
            this.props.finishEdit()
    }

    renderView(task: Task) {
        if (this.props.isEdit) {
            return <input className="edit" defaultValue={task.description} onKeyUp={this.finishEdit}/>;
        } else {
            return (
                <div className="view">
                    <input className="toggle" type="checkbox" checked={task.done} onClick={this.toggleDone}/>
                    <label onDoubleClick={this.edit}>{task.description}</label>
                    <button className="destroy" onClick={this.remove}/>
                </div>
            );
        }
    }

    render() {
        let task = this.props.task;
        let className = ''
        if (task.done)
            className += ' completed'
        if (this.props.isEdit)
            className += ' editing'

        return (
            <li className={className}>
                {this.renderView(task)}
            </li>
        );
    }
}