import * as React from 'react';
import {TodoViewType} from "../constants/todo";

export default class TodoFilter extends React.Component<any,any> {
    selected() {
        return this.props.currentView == this.props.view;
    }

    render() {
        return (
            <li>
                <a className={this.selected() && 'selected'} href="#/"
                   onClick={()=>this.props.toggleView(this.props.view)}>{TodoViewType[this.props.view]}</a>
            </li>
        )
    };
}