import * as React from 'react';
import TodoFilter from "./TodoFilter";
import {TodoViewType} from "../constants/todo";

export default class TodoFooter extends React.Component<any,any> {
    constructor(props) {
        super(props)
    }

    render() {
        let desc = this.props.todoCount == 1 ? 'item' : 'items';

        return (
            <footer className="footer">
                <span className="todo-count"><strong>{this.props.todoCount}</strong> {desc} left</span>
                <ul className="filters">
                    <TodoFilter view={TodoViewType.ALL} toggleView={this.props.toggleView.bind(this)}
                                currentView={this.props.currentView}/>
                    <TodoFilter view={TodoViewType.ACTIVE} toggleView={this.props.toggleView.bind(this)}
                                currentView={this.props.currentView}/>
                    <TodoFilter view={TodoViewType.COMPLETED} toggleView={this.props.toggleView.bind(this)}
                                currentView={this.props.currentView}/>
                </ul>
                <button className="clear-completed" onClick={()=>this.props.clearCompleted()}>Clear completed</button>
            </footer>
        );
    }
}