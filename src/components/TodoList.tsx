import * as React from 'react';
import {Task, TaskCollection} from "../models/todo";
import AddTaskView from "./AddTaskView";
import TaskListRow from "./TaskListRow";
import TodoFooter from "./TodoFooter";
import {TodoProperty, TodoState} from "../definitions/todo";
import {TodoViewType} from "../constants/todo";

export default class TodoList extends React.Component<TodoProperty, TodoState> {
    constructor(props) {
        super(props)
        this.state = {tasks: new TaskCollection(), currentView: TodoViewType.ALL, editId: -1}
        this.addTask = this.addTask.bind(this)
        this.toggleDone = this.toggleDone.bind(this)
        this.remove = this.remove.bind(this)
        this.toggleView = this.toggleView.bind(this)
        this.clearCompleted = this.clearCompleted.bind(this)
        this.toggleAll = this.toggleAll.bind(this)
        this.edit = this.edit.bind(this)
        this.finishEdit = this.finishEdit.bind(this)
    }

    addTask(description) {
        this.state.tasks.add(new Task(description))
        this.setState((prevState) => ({tasks: prevState.tasks}))
    }

    toggleDone(id: number) {
        this.state.tasks.find(id).toggleDone()
        this.setState((prevState) => ({tasks: prevState.tasks}))
    }

    remove(id: number) {
        this.state.tasks.removeById(id)
        this.setState((prevState) => ({tasks: prevState.tasks}))
    }

    toggleView(view: TodoViewType) {
        this.setState({currentView: view})
    }

    toggleAll(e) {
        this.state.tasks.forEach((task) => {
            task.done = e.target.checked;
        })
        this.setState((prevState) => ({tasks: prevState.tasks}))
    }

    clearCompleted() {
        this.setState({tasks: this.state.tasks.findByStatus(false), currentView: TodoViewType.ALL})
    }

    edit(id: number) {
        this.setState({editId: id})
    }

    finishEdit(task?: Task) {
        if (task)
            this.state.tasks.edit(task)
        this.setState((prevState) => ({tasks: prevState.tasks, editId: -1}))
    }

    tasksInView() {
        if (this.state.currentView === TodoViewType.ALL)
            return this.state.tasks;
        else if (this.state.currentView === TodoViewType.ACTIVE)
            return this.state.tasks.pending()
        else if (this.state.currentView === TodoViewType.COMPLETED)
            return this.state.tasks.completed()
    }

    render() {
        const tasks = this.tasksInView();
        return (
            <section className="todoapp">
                <header className="header">
                    <h1>todos</h1>
                    <AddTaskView addTask={this.addTask}/>
                </header>

                <section className="main">
                    <input className="toggle-all" type="checkbox" onClick={this.toggleAll}/>
                    <ul className="todo-list">
                        {tasks.map((task, index) => {
                            return <TaskListRow
                                task={task}
                                key={index}
                                toggleDone={this.toggleDone}
                                remove={this.remove}
                                isEdit={this.state.editId == task.id}
                                edit={this.edit}
                                finishEdit={this.finishEdit}
                            />
                        })}
                    </ul>
                </section>

                {!this.state.tasks.empty() &&
                <TodoFooter todoCount={this.state.tasks.pending().count()} currentView={this.state.currentView}
                            toggleView={this.toggleView} clearCompleted={this.clearCompleted}/>
                }
            </section>
        );
    }
}
