import {TodoViewType} from "../constants/todo";
import {TaskCollection} from "../models/todo";

interface TodoProperty {

}

interface TodoState {
    editId?: number;
    tasks?: TaskCollection;
    currentView?: TodoViewType;
}