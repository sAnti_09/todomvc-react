export class TaskCollection {
    tasks: Task[];

    constructor(tasks: Task[] = []) {
        this.tasks = tasks;
    }

    add(task: Task) {
        task.id = this.tasks.length + 1
        this.tasks.push(task)
    }

    remove(task: Task) {
        for (let index = 0; index < this.tasks.length; index++) {
            if (this.tasks[index].id == task.id) {
                this.tasks.splice(index, 1)
                break;
            }
        }
    }

    removeById(id: number) {
        this.remove(this.find(id));
    }

    find(id: number) {
        const results = this.tasks.filter((task) => task.id == id)
        if (results.length)
            return results[0];
        return null;
    }

    edit(task: Task) {
        for (let index = 0; index < this.tasks.length; index++) {
            if (this.tasks[index].id == task.id) {
                this.tasks[index] = task
                break;
            }
        }
    }

    count() {
        return this.tasks.length;
    }

    pending() {
        return this.findByStatus(false)
    }

    completed() {
        return this.findByStatus(true)
    }

    findByStatus(done: boolean) {
        return new TaskCollection(this.tasks.filter((task) => task.done == done))
    }

    map<U>(mapFunction: (task: Task, index: number)=> U) {
        return this.tasks.map(mapFunction);
    }

    forEach<U>(mapFunction: (task: Task, index: number)=> U) {
        return this.tasks.forEach(mapFunction);
    }

    empty() {
        return this.tasks.length == 0;
    }

}
export class Task {
    id: number;
    description: string;
    done: boolean;
    removed: boolean;

    constructor(description: string, done: boolean = false, removed: boolean = false) {
        this.description = description;
        this.done = done;
        this.removed = removed;
    }

    toggleDone() {
        this.done = !this.done
    }
}