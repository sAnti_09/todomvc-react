const autoprefixer = require('autoprefixer')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const path = require('path')

const sassLoaders = [
  'css-loader',
  'postcss-loader',
  'sass-loader'
]

const outputDir = '/js';
const fontsDir = '../fonts2'
const cssDir = '../css'

const config = {
  entry: {
    hello: ['src/hello.tsx']
  },
  devtool: "source-map",
  module: {
    loaders: [
      // {
      //   test: /\.js$/,
      //   exclude: /node_modules/,
      //   loaders: ['babel-loader']
      // },
      {test: /\.tsx?$/, loader: "ts-loader"},
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', sassLoaders.join('!'))
      },
      {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&mimetype=application/font-woff&name=" + fontsDir + "/[hash].[ext]"
      }, {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&mimetype=application/font-woff&name=" + fontsDir + "/[hash].[ext]"
      }, {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&mimetype=application/octet-stream&name=" + fontsDir + "/[hash].[ext]"
      }, {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file?name=" + fontsDir + "/[hash].[ext]"
      }, {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&mimetype=image/svg+xml&name=" + fontsDir + "/[hash].[ext]"
      }
    ]
  },
  output: {
    filename: '[name].js',
    path: path.join(__dirname, outputDir),
  },
  plugins: [
    new ExtractTextPlugin(cssDir + '/[name].css')
  ],
  postcss: [
    autoprefixer({
      browsers: ['last 2 versions']
    })
  ],
  // sassLoader: [{
  //   includePaths: [path.resolve(__dirname, "/scss")]
  // }],
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js'],
    root: [path.join(__dirname)]
  },
  externals: {
    "react": "React",
    "react-dom": "ReactDOM"
  }
}

module.exports = config